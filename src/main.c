#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>

/**
 * Variable declarations
 */
#define CHAR_WIDTH 8
#define CHAR_HEIGHT 16

#define NUM_COLUMNS 80
#define NUM_ROWS 24

#define COLUMN_PADDING 16
#define ROW_PADDING 32

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#define SCREEN_BPP 32

#define SCREEN_FPS 500
#define SCREEN_TICKS_PER_FRAME 1000 / SCREEN_FPS

static SDL_Color DEFAULT_TEXT_COLOR = {0xFF, 0x00, 0xFF};

// The window to render
SDL_Window* gWindow = NULL;

// The window renderer
SDL_Renderer* gRenderer = NULL;

// The surface contained in the window
SDL_Surface* gScreenSurface = NULL;

// The texture using the specified sprite
SDL_Texture* gCurrentSpriteTexture = NULL;

// The texture for the text rendered
SDL_Texture* gTextTexture = NULL;

// Current screen width/height
int gWindowWidth = CHAR_WIDTH * NUM_COLUMNS + COLUMN_PADDING;
int gWindowHeight = CHAR_HEIGHT * NUM_ROWS + ROW_PADDING;

// The text font in use
TTF_Font* gFont = NULL;

// Width/height of text surface
int gTextWidth = 0;
int gTextHeight = 0;

// Mappings for sprite surfaces per arrow key event
enum KeyEventType
{
  KEY_PRESS_DEF,
  KEY_PRESS_UP,
  KEY_PRESS_DOWN,
  KEY_PRESS_LEFT,
  KEY_PRESS_RIGHT,
  KEY_PRESS_COUNT,
};

SDL_Texture* gKeyPressTextures[KEY_PRESS_COUNT];

char* IMAGE_LOCATIONS[KEY_PRESS_COUNT] = {
  "/path/to/default.bmp",
  "/path/to/up.bmp",
  "/path/to/down.bmp",
  "/path/to/left.bmp",
  "/path/to/right.bmp",
};

// Default location for font loading
char* FONT_LOCATION = "/path/to/font.ttf";

/**
 * Function prototypes
 */
int init();
int loadImage(char* imagePath, SDL_Texture** texturePtr);
int loadFont(char* fontPath, int fontSize);
int renderText(const char* text, SDL_Color textColor);
void runMainLoop();
void close();
void cleanupTextures();


/**
 * Create a basic window via SDL2 libraries.
 *
 * @param argc The argument count.
 * @param argv The argument list.
 * @return The exit code of the process.
 * @unsafe Creating windows via libraries interacting w/ OS.
 * Naturally effectful for the environment. What could possibly go wrong?
 */
int
main(int argc, char* argv[])
{
  // Initialize SDL resources
  if (init() != 0) {
    return -1;
  }

  // Main program to draw screen with sprite
  runMainLoop();

  // Close SDL resources
  close();
  return 0;
}

/**
 * Initialize SDL components (including window & surface).
 *
 * @return 0 exit code for success; non-zero code for failure.
 * @unsafe Environment must maintain gui app components.
 */
int
init()
{
  // Initialize SDL components
  if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
    printf("Error initializing SDL! SDL_Error: %s\n", SDL_GetError());
    return -1;
  }

  // Create window
#if 0
  gWindow = SDL_CreateWindow(
    "Hello SDL!",
    SDL_WINDOWPOS_UNDEFINED,
    SDL_WINDOWPOS_UNDEFINED,
    CHAR_WIDTH * NUM_COLUMNS + COLUMN_PADDING,
    CHAR_HEIGHT * NUM_ROWS + ROW_PADDING,
    SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN_DESKTOP
    );
#else
  gWindow = SDL_CreateWindow(
    "Hello SDL!",
    SDL_WINDOWPOS_UNDEFINED,
    SDL_WINDOWPOS_UNDEFINED,
    gWindowWidth,
    gWindowHeight,
    SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE
    );
#endif

  if (gWindow == NULL) {
    printf("Cannot create SDL window! SDL_Error: %s\n", SDL_GetError());
    return -1;
  }
 
  gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
  if (gRenderer == NULL) {
    printf("Cannot create SDL renderer! SDL_Error: %s\n", SDL_GetError());
    return -1;
  }

  // TODO: Initialize PNG loading using IMG_Init

  // Retrieve surface from window
  gScreenSurface = SDL_GetWindowSurface(gWindow);

  // Load key press images
  for (int i = 0; i < KEY_PRESS_COUNT; i++) {
    if (loadImage(IMAGE_LOCATIONS[i], &gKeyPressTextures[i]) != 0) {
      while (--i >= 0) {
        SDL_DestroyTexture(gKeyPressTextures[i]);
        gKeyPressTextures[i] = NULL;
      }
      return -1;
    }
  }

  // Initialize TTF protocols
  if (TTF_Init() == -1) {
    printf("Cannot initialize ttf functionality! SDL_Error: %s\n", TTF_GetError());
    return -1;
  }

  if (loadFont(FONT_LOCATION, CHAR_WIDTH) != 0) {
    printf("Cannot load font! %s\n", FONT_LOCATION);
    return -1;
  }

  return 0;
}

/**
 * Load image in SDL texture.
 *
 * @param imagePath The image source on the machine.
 * @param texturePtr The relative pointer to the SDL texture struct.
 * @return 0 exit code for success; non-zero code for failure.
 * @unsafe Environment must maintain texture for window & image.
 * Image load from filesystem might fail and prompt SDL error.
 */
int
loadImage(char* imagePath, SDL_Texture** texturePtr)
{
  SDL_Surface* loadedSurface = SDL_LoadBMP(imagePath);
  if (loadedSurface == NULL) {
    printf("Could not load image %s! SDL Error: %s\n", imagePath, SDL_GetError());
    return -1;
  }

  *texturePtr = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
  SDL_FreeSurface(loadedSurface);

  if (*texturePtr == NULL) {
    printf("Could not optimize image %s! SDL Error: %s\n", imagePath, SDL_GetError());
    return -1;
  }

  return 0;
}

/**
 * Load font in SDL ttf context object.
 *
 * @param fontPath The path containing the font to be loaded.
 * @param fontSize The size of the font to be rendered for future use.
 * @return 0 exit code for success; non-zero code for failure.
 * @unsafe Environment must maintain texture and file for font.
 * Font load from filesystem might fail and prompt SDL error.
 */
int
loadFont(char* fontPath, int fontSize)
{
  gFont = TTF_OpenFont(fontPath, fontSize * 4);
  if (gFont == NULL) {
    printf("Failed to load font! SDL_ttf error: %s\n", TTF_GetError());
    return -1;
  }

  return 0;
}

/**
 * Present text texture with text to render in gui.
 *
 * @param text The text to present to the text texture.
 * @param textColor The color of the text to use in the text texture.
 * @return 0 exit code for success; non-zero code for failure.
 * @unsafe Environment must maintain texture for gui text.
 *
 */
int
renderText(const char* text, SDL_Color textColor)
{
  SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, text, textColor);
  if (textSurface == NULL) {
    return -1;
  }

  int errCode;
  SDL_DestroyTexture(gTextTexture);
  gTextTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);

  if (gTextTexture == NULL) {
    printf("Could not create texture from text surface! SDL Error: %s\n", SDL_GetError());
    errCode = -1;
  } else {
    gTextWidth = textSurface->w;
    gTextHeight = textSurface->h;

    errCode = 0;
  }

  SDL_FreeSurface(textSurface);
  return errCode;
}

/**
 * The main gui program displaying images per key press events.
 * @unsafe Environment must maintain surface for window & image.
 * Window surface changes depending on keyboard inputs.
 */
void
runMainLoop()
{
  SDL_Event e;
  gCurrentSpriteTexture = gKeyPressTextures[KEY_PRESS_DEF];

  // Initialize base stretched image
  SDL_Rect stretchRect;
  stretchRect.x = 0;
  stretchRect.y = 0;
  stretchRect.w = gWindowWidth;
  stretchRect.h = gWindowHeight;

  // TODO: bigger text buffer for proper rendering on scrollback/text input
  char tinyTextBuffer[2] = {' ', '\0'};
  unsigned char keyMap[128] = { 0 };
  unsigned char shiftTable[96] =
  {
    ' ', '!', '"', '#', '$', '%', '&', '"',  // last key = '
    '(', ')', '*', '+', '<', '_', '>', '?',  // (, ), *, +, ',', -, ., /
    ')', '!', '@', '#', '$', '%', '^', '&',  // 0-7
    '*', '(', ':', ':', '<', '+', '>', '?',  // 8-9, ;, :, <, =, >, ?
    '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
    'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
    'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
    'X', 'Y', 'Z', '{', '|', '}', '^', '_',  // X-Z, [, backslash, ], ^, -
    '~', 'A', 'B', 'C', 'D', 'E', 'F', 'G',  // `, a-g
    'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',  // h-o
    'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',  // p-w
    'X', 'Y', 'Z', '{', '|', '}', '~', 0,   // x-z, ..., DELETE key
  };

  int quit = 0;
  int dirty = 1;
  int displayText = 0;

  while (!quit) {
    Uint32 startTicks = SDL_GetTicks();

    while (SDL_PollEvent(&e) != 0) {
      switch (e.type) {
        case SDL_QUIT:
        // Exit with blank teal screen
        gCurrentSpriteTexture = NULL;

        // When process close (ctrl+c) or 'X' is clicked in window
        // Signal screen redraw
        dirty |= 1;

        // Non-white fill on surface
        // Renderer color setup
        SDL_SetRenderDrawColor(gRenderer, 0x00, 0xFF, 0xFF, 0xFF);

        quit = 1;
        break;
        case SDL_WINDOWEVENT:
        // TOOD: handle window resizing events
        switch (e.window.event) {
        case SDL_WINDOWEVENT_MAXIMIZED:
        case SDL_WINDOWEVENT_RESIZED:
        case SDL_WINDOWEVENT_SIZE_CHANGED:
          dirty |= 1;
          gWindowWidth = e.window.data1;
          gWindowHeight = e.window.data2;

          stretchRect.w = gWindowWidth;
          stretchRect.h = gWindowHeight;

          // Retrieve surface from window
          gScreenSurface = SDL_GetWindowSurface(gWindow);

          // White fill on surface
          SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

          // Apply the sprite to the window
          SDL_BlitScaled(gScreenSurface, NULL, gScreenSurface, &stretchRect);
          break;
        default:
          break;
        }
        break;
        case SDL_KEYUP:
        case SDL_KEYDOWN:
        // Signal combination of keys pressed/released
        if (e.key.keysym.sym > 0 && e.key.keysym.sym <= SDLK_DELETE) {
          keyMap[e.key.keysym.sym] = e.type == SDL_KEYDOWN;

          // Signal screen redraw
          dirty |= 1;
        }

        // Signal that text can be drawn on screen
        if ((e.key.keysym.sym >= SDLK_SPACE && e.key.keysym.sym <= SDLK_DELETE)) {
          unsigned int isShifted = e.key.keysym.mod & KMOD_SHIFT;

          // TODO: ctl, alt, gui keys
          if (e.key.keysym.mod & KMOD_CAPS) {
            if (e.key.keysym.sym >= SDLK_a && e.key.keysym.sym <= SDLK_z) {
              if (isShifted) {
                tinyTextBuffer[0] = e.key.keysym.sym;
              } else {
                tinyTextBuffer[0] = shiftTable[e.key.keysym.sym - ' '];
              }
            } else if (isShifted) {
              tinyTextBuffer[0] = shiftTable[e.key.keysym.sym - ' '];
            } else {
              tinyTextBuffer[0] = e.key.keysym.sym;
            }
          } else if (isShifted) {
            tinyTextBuffer[0] = shiftTable[e.key.keysym.sym - ' '];
          } else {
            tinyTextBuffer[0] = e.key.keysym.sym;
          }

          if (!keyMap[e.key.keysym.sym]) {
            gCurrentSpriteTexture = gKeyPressTextures[KEY_PRESS_DEF];

            // White fill on surface
            SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

            // Apply the sprite to the window
            SDL_BlitScaled(gScreenSurface, NULL, gScreenSurface, &stretchRect);
          } else {
            int textRendered = renderText(tinyTextBuffer, DEFAULT_TEXT_COLOR);

            if (textRendered != 0) {
              dirty = 0;
              quit = 1;
              displayText = 0;
            } else {
              displayText |= 1;
              gCurrentSpriteTexture = gTextTexture;

              // White fill on surface
              SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

              // Apply the sprite to the window
              SDL_BlitScaled(gScreenSurface, NULL, gScreenSurface, &stretchRect);
            }
          }
        } else {
          // Signal screen redraw
          dirty |= 1;

          switch (e.key.keysym.sym) {
          case SDLK_RIGHT:
            gCurrentSpriteTexture = gKeyPressTextures[KEY_PRESS_RIGHT];
            break;

          case SDLK_LEFT:
            gCurrentSpriteTexture = gKeyPressTextures[KEY_PRESS_LEFT];
            break;

          case SDLK_DOWN:
            gCurrentSpriteTexture = gKeyPressTextures[KEY_PRESS_DOWN];
            break;

          case SDLK_UP:
            gCurrentSpriteTexture = gKeyPressTextures[KEY_PRESS_UP];
            break;

          default:
            gCurrentSpriteTexture = gKeyPressTextures[KEY_PRESS_DEF];
            break;
          }
        }

        // White fill on surface
        SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

        // Apply the sprite to the window
        SDL_BlitScaled(gScreenSurface, NULL, gScreenSurface, &stretchRect);
        break;
        default:
        // White fill on surface
        SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

        // Apply the sprite to the window
        SDL_BlitScaled(gScreenSurface, NULL, gScreenSurface, &stretchRect);
        break;
      }
    }

    // Surface update only when necessary
    if (dirty) {
      // Clear screen
      SDL_RenderClear(gRenderer);

      // Update window surface underneath texture
      SDL_UpdateWindowSurface(gWindow);

      if (displayText) {
        // Render text
        SDL_Rect renderQuad = { gWindowWidth / 2, gWindowHeight / 2, gTextWidth, gTextHeight };
        SDL_RenderCopy(gRenderer, gCurrentSpriteTexture, NULL, &renderQuad);

        displayText = 0;
      } else {
        // Render regular texture
        SDL_RenderCopy(gRenderer, gCurrentSpriteTexture, NULL, NULL);
      }

      // Update screen
      SDL_RenderPresent(gRenderer);

      dirty = 0;
    }

    // Slow down per FPS
    Uint32 frameTicks = SDL_GetTicks() - startTicks;
    if (frameTicks < SCREEN_TICKS_PER_FRAME) {
      SDL_Delay(SCREEN_TICKS_PER_FRAME - frameTicks);
    }
  }

  // Two second waiting time
  SDL_Delay(2000);
}

/**
 * Clean up program resources, including SDL structs.
 */
void
close()
{
  // Sprite surface cleanup
  cleanupTextures();
  gCurrentSpriteTexture = NULL;

  // Clean up font assets
  TTF_CloseFont(gFont);
  gFont = NULL;

  // Window cleanup
  SDL_DestroyRenderer(gRenderer);
  SDL_DestroyWindow(gWindow);
  gWindow = NULL;
  gRenderer = NULL;

  // Retreat from SDL subsystems
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();
}

/**
 * Helper function to clean up SDL surfaces linked to key presses.
 */
void
cleanupTextures()
{
  for (int i = 0; i < KEY_PRESS_COUNT; i++) {
    SDL_DestroyTexture(gKeyPressTextures[i]);
    gKeyPressTextures[i] = NULL;
  }
}

